/*
 * HeadStore - Bundle compressed binary files into C++ programs
 *
 * Written in 2018 by joonatoona
 *
 * To the extent possible under law, the author(s) have dedicated all copyright
 * and related and neighboring rights to this software to the public domain
 * worldwide. This software is distributed without any warranty.
 *
 * You should have received a copy of the CC0 Public Domain Dedication along
 * with this software. If not, see
 * <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#include <zlib.h>
#include <cstring>
#include <fstream>
#include <iostream>
#include <sstream>

int main(int argc, char *argv[]) {
    if (argc < 2) {
        std::cerr << "Usage: " << argv[0] << " <file to headify>" << std::endl;
        return -1;
    }

    std::ifstream in(argv[1], std::ios::binary);

    in.seekg(0, std::ios::end);
    ulong inLen = in.tellg();
    in.seekg(0, std::ios::beg);

    char buff[inLen];
    in.read(buff, inLen);
    in.close();

    ulong compSize = compressBound(inLen);
    char cBuff[compSize];

    compress((Bytef *)cBuff, &compSize, (Bytef *)buff, inLen);

    std::stringstream sstream;

    for (int i = 0; i < compSize; i++) sstream << (int)cBuff[i] << ", ";

    std::string byteArray = sstream.str();
    byteArray = byteArray.substr(0, byteArray.length() - 2);
    std::cout << byteArray;
    return 0;
}
